%% LaTeX2e class for student theses
%% sdthesis.cls
%%
%% Karlsruhe Institute of Technology
%% Institute for Program Structures and Data Organization
%% Chair for Software Design and Quality (SDQ)
%%
%% Dr.-Ing. Erik Burger
%% burger@kit.edu
%%
%% See https://sdqweb.ipd.kit.edu/wiki/Dokumentvorlagen
%%
%% Version 1.3.5, 2020-06-26

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{hccthesis}[2020-06-26 v1.3.5 Student thesis class]
% Language options. German is also needed in English thesis for the abstract
\DeclareOption{ngerman}{\PassOptionsToPackage{\CurrentOption}{babel}}
\DeclareOption{english}{\PassOptionsToPackage{main=\CurrentOption,ngerman}{babel}}
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{scrbook}}
\ProcessOptions\relax
% Used to detect language families
\RequirePackage{translations}

%% ---------------------------------------------------------------
%% | Based on KOMAscript by Markus Kohm http://www.komascript.de |
%% ---------------------------------------------------------------
\LoadClass{scrbook}

%% --------------
%% | Typography |
%% --------------

% T1 font encoding
\RequirePackage[T1]{fontenc}
\RequirePackage[utf8]{inputenc}

% % serif type: Linux Libertine
% \RequirePackage{libertine}
% % Linux Libertine in math mode
% \RequirePackage[libertine]{newtxmath}

% % grotesque type: Adobe Source Sans Pro
% \RequirePackage[scaled=.92]{sourcesanspro}

% % monospace type: Bera Mono
% \RequirePackage[scaled=.78]{beramono}

% % semi-bold type (for subsections and paragraphs)
% \newcommand*{\sbdefault}{sb}
% \DeclareRobustCommand{\sbseries}{%
%   \not@math@alphabet\sbseries\relax
%   \fontseries\sbdefault\selectfont}

% \DeclareTextFontCommand{\textsb}{\sbseries}

% \addtokomafont{subsection}{\sbseries}
% \addtokomafont{subsubsection}{\sbseries}
% \addtokomafont{paragraph}{\sbseries}

% microtype for nicer typography
\RequirePackage[protrusion=true,expansion=true]{microtype}

% commands for code and model keywords in text
\newcommand{\code}[1]{\texttt{\hyphenchar\font45\relax #1}}
\newcommand{\model}[1]{\textsf{#1}}

% nicer spacing for enumerations
\RequirePackage{enumitem}

%% ---------------
%% | Page Layout |
%% ---------------
\KOMAoptions{
   fontsize=12pt,
   paper=a4,
   titlepage=true,
   headinclude=true,
   headings=small,
   footinclude=false,
   DIV=calc,
   BCOR=5mm
}

% Margins
\if@twoside%
  \typearea[20mm]{15} % twoside
\else% 
  \typearea[10mm]{15} % oneside
\fi%
\RequirePackage[headsepline,draft=false]{scrlayer-scrpage}
\pagestyle{scrheadings}

\clubpenalty=10000 %prevent orphans
\widowpenalty=10000 %prevent widows


%% --------------
%% | Title page |
%% --------------

% Logos
%% it is not permitted to use the logo of the Freie Universität Berlin on theses.
\newcommand{\thegrouplogo}{HCC-logo-wide}
\newcommand{\printgrouplogo}{\includegraphics[height=1.8cm]{logos/\thegrouplogo}}
\newcommand{\nogrouplogo}{\renewcommand{\printgrouplogo}{}}

%
% \setkomafont{title}{\large\sffamily\bfseries}
% \setkomafont{subtitle}{\normalfont\large}

% \titlehead{
% \large 
% \thethesistype ~\ifcurrentbaselanguage{English}{at the Institute for Computer Science at the Freie Universität Berlin}%
% {am Institut für Informatik an der Freien Universität Berlin} \\
% \theinstitute\\[2em]
% }

%% variables for title page

\newcommand{\theinstitute}{Human-Centered Computing (HCC)}
\newcommand{\thethesistype}{}
\newcommand{\thethesistitle}{}
\newcommand{\thereviewerone}{}
\newcommand{\thereviewertwo}{}
\newcommand{\theadvisorone}{}
\newcommand{\theadvisortwo}{}
\newcommand{\theeditstart}{}
\newcommand{\theeditend}{}

%% formatting commands for titlepage
\newcommand{\thesistype}[1]{\renewcommand{\thethesistype}{#1}}
\newcommand{\thesistitle}[1]{\renewcommand{\thethesistitle}{#1}}
\newcommand{\myinstitute}[1]{\renewcommand{\theinstitute}{#1}}
\newcommand{\reviewerone}[1]{\renewcommand{\thereviewerone}{#1}}
\newcommand{\reviewertwo}[1]{\renewcommand{\thereviewertwo}{#1}}
\newcommand{\advisorone}[1]{\renewcommand{\theadvisorone}{#1}}
\newcommand{\advisortwo}[1]{\renewcommand{\theadvisortwo}{#1}}


\newcommand{\thestudentname}{}
\newcommand{\thestudentmail}{}
\newcommand{\thestudentnumber}{}

\newcommand{\student}[3]{%
\renewcommand{\thestudentname}{#1}%
\renewcommand{\thestudentmail}{#2}%
\renewcommand{\thestudentnumber}{\ifcurrentbaselanguage{English}{Matriculation Number}%
{Matrikelnummer}: #3}%
%% set the author
\author{\thestudentname \\ \thestudentmail \\ \thestudentnumber}
}

\newcommand{\editingtime}[2]{%
\renewcommand{\theeditstart}{#1}%
\renewcommand{\theeditend}{#2}%
%% do not show the date
\date{}
}

%% -----------------------------
%% | Abstract/Acknowledgements |
%% -----------------------------

\newcommand{\abstract}[1][\abstractname]{\chapter*{#1}}
\newcommand{\Abstract}[1][\abstractname]{\chapter*{#1}\addcontentsline{toc}{chapter}{#1}}

\def\ackname{Acknowledgments}
\def\abstractname{Abstract}
\def\switcht@deutsch{\svlanginfo
	\def\ackname{Danksagung}
	\def\abstractname{Kurzfassung}
}
\def\switcht@english{\svlanginfo
	\def\ackname{Acknowledgements}
	\def\abstractname{Abstract}
}

%% In English theses, an additional German Abstract is needed.
\newcommand{\includeabstract}{
\ifcurrentbaselanguage{English}{
% include English and German abstracts
\input{sections/abstract_en.tex}
\begin{otherlanguage}{ngerman}
\input{sections/abstract_de.tex}
\end{otherlanguage}
}{
% include only German abstract
\include{sections/abstract_de}
}
}

%% ------------
%% | Packages |
%% ------------

% draft mode
\RequirePackage{ifdraft}
\RequirePackage{ifthen}

% enumerate subsubsections
\setcounter{secnumdepth}{3}

% subimport of files
\RequirePackage{import}

% languages
\RequirePackage{babel}
\RequirePackage{csquotes}

% hyphenation for compound words
\RequirePackage[shortcuts]{extdash}

% nice tables
\RequirePackage{booktabs}
\RequirePackage{longtable}
\RequirePackage{array}

% show graphics in draft mode
\RequirePackage{graphicx}
\setkeys{Gin}{draft=false}

% appendix
\RequirePackage[toc,title,header]{appendix}
\noappendicestocpagenum

% PDF specific packages
\RequirePackage[hyphens]{url}
\RequirePackage[breaklinks,colorlinks=false,pdfpagelabels]{hyperref}
\newcommand\setpdf{
    \hypersetup{%
    pdftitle={\thethesistype},%
    pdfsubject={\thethesistitle},%
    pdfauthor={\thestudentname},%
    pdfborder={0 0 0},%
    }%
    \let\theauthor\thestudentname
}